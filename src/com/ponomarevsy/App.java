package com.ponomarevsy;

import java.io.*;
import java.sql.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.*;

/**
 * Created by ��������� ��������� on 22.10.15.
 */
public class App {

    private String url;
    private String user;
    private String password;
    private int numberN;
    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;
    private final String QUERY_SELECT_COUNT = "select count(*) from test";
    private final String QUERY_SELECT = "select field from test";
    private final String QUERY_DELETE = "truncate table test";
    private String query_insert_template = "insert into test(field) values ";

    public App() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNumberN() {
        return numberN;
    }

    public void setNumberN(int numberN) {
        this.numberN = numberN;
    }

    public void insertToTestTableAndWriteToXmlFromTestTable() {
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            resultSet= statement.executeQuery(QUERY_SELECT_COUNT); //���������, ������ �� �������
            resultSet.next();
            int count = resultSet.getInt(1);
            if(count != 0) {
                statement.execute(QUERY_DELETE); // ���� �� ������, �������
            }
                // ������� ������ sql ��� ������� � �������
            StringBuilder builder = new StringBuilder();
            for(int i = 1; i <= numberN; i++) {
            builder.append( "(" + i + ")" + ",");
            }
            String query_insert = query_insert_template + builder.toString();
            query_insert = query_insert.substring(0 , query_insert.lastIndexOf(",")) + ";";
            statement.execute(query_insert); // ��������� ������� � �������
            //�������� ������ �� �������
            resultSet = statement.executeQuery(QUERY_SELECT);
            //������� ��������� ����� xml
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element entries = document.createElement("entries");
            document.appendChild(entries);
            ResultSetMetaData resultMeta = resultSet.getMetaData();
            int colCount = resultMeta.getColumnCount();
            while (resultSet.next()) {
                Element entry = document.createElement("entry");
                entries.appendChild(entry);
                for (int i = 1; i <= colCount; i++) {
                    String columnName = resultMeta.getColumnName(i);
                    Object value = resultSet.getObject(i);
                    Element node = document.createElement(columnName);
                    node.appendChild(document.createTextNode(value.toString()));
                    entry.appendChild(node);
                }
            }
            DOMSource domSource = new DOMSource(document);
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            StringWriter stringWriter = new StringWriter();
            StreamResult streamResult = new StreamResult(stringWriter);
            transformer.transform(domSource, streamResult);
          //  ������� ���� � ���������� � ���� xml ����������
            FileWriter writer = new FileWriter(new File("C:/1.xml"));
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            writer.write(stringWriter.toString());
            writer.flush();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            try { connection.close(); } catch(Exception e) {e.printStackTrace(); }
            try { statement.close(); } catch(Exception e) { e.printStackTrace(); }
            try { resultSet.close(); } catch(Exception e) { e.printStackTrace(); }
        }
    }

    public void convertXmlFile() {
        try {
            //������� xslt ������ � ���������� ��� �� ����
            FileWriter writer = new FileWriter(new File("C:/0.xslt"));
            String xsltTemplate = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                    "\n" +
                    "<xsl:stylesheet version=\"1.0\"\n" +
                    "\n" +
                    "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n" +
                    "\n" +
                    "<xsl:output method=\"xml\" />\n" +
                    "\n" +
                    "<xsl:template match=\"/\">\n" +
                    "<entries>\n" +
                    "\n" +
                    "<xsl:apply-templates />\n" +
                    "\n" +
                    "</entries>\n" +
                    "\n" +
                    "</xsl:template>\n" +
                    "\n" +
                    "<xsl:template match=\"entry\">\n" +
                    "\n" +
                    "<entry field=\"{field}\">\n" +
                    "\n" +
                    "</entry>\n" +
                    "\n" +
                    "</xsl:template>\n" +
                    "\n" +
                    "</xsl:stylesheet> ";
            writer.write(xsltTemplate);
            writer.flush();
            // ������� ����������� xsl �� ������ ���������� �������
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer(new StreamSource("C:/0.xslt"));
            // ��������������
            transformer.transform(new StreamSource("C:/1.xml"), new StreamResult("C:/2.xml"));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void parseXmlAndPrintSum() {
        try{
            // ������� ������
            DocumentBuilder documentBuilder= DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(new File("C:/2.xml"));
            // �������� ������������ �������
            Element rootElement = document.getDocumentElement();
            // �������� ������ �������� ��������� ������� ������
            Node entries = rootElement.getFirstChild();
            long sum = 0;
            do {
                NamedNodeMap nodeMap = entries.getAttributes();
                sum += Integer.parseInt(nodeMap.getNamedItem("field").getTextContent());
                entries = entries.getNextSibling();
            } while(entries != null);
            System.out.println("Sum = " + sum);  // ������� ����� ���� ��������� field ������� �������� entry
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis(); // ��������� ����� ������ ������ ����������
        App app = new App();
       // app.setUrl("jdbc:mysql://localhost:3306/test");
        app.setUrl(args[0]); // ������������� ���� � ���� ������
       // app.setUser("root");
        app.setUser(args[1]); // ������������� ��� ������������
       // app.setPassword("root");
        app.setPassword(args[2]); // ������������� ������
       // app.setNumberN(10000);
        app.setNumberN(Integer.parseInt(args[3]));  // ������������� ����� N
        if(app.getNumberN() < 1) return; // ���� N < 1, ���������� ������
        app.insertToTestTableAndWriteToXmlFromTestTable(); // ���������� � �������, ���������� � ���� �� �������
        app.convertXmlFile(); // ��������������� ����
        app.parseXmlAndPrintSum(); // ������ ���� � ������� �����
        long endTime = System.currentTimeMillis(); // ��������� ����� ��������� ������ ����������
        long milliseconds = endTime - startTime;
        String time = String.format("Working time is %d minutes %d seconds", milliseconds/60000, (milliseconds%60000)/1000);
        System.out.println(time); // ������� ����� ������ ����������
    }


}
