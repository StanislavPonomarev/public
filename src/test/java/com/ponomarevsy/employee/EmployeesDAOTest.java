package com.ponomarevsy.employee;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@ContextConfiguration(locations = {"/applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeesDAOTest {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private EmployeesDAO employeesDAO;

    @Test
    public void testGetById() throws Exception {
        Employee employee = employeesDAO.getById(1);
        assertEquals("Ivan" , employee.getFirstName());
    }

    @Test
    public void testSearchEmployees() throws Exception {
        List<Employee> employees = employeesDAO.searchEmployees("Petr");
        assertTrue(employees.size() > 0);
        assertEquals("Petr" , employees.get(0).getFirstName());
    }

    @Test
    public void testGetAllEmployees() throws Exception {
        List<Employee> employees = employeesDAO.getAllEmployees(1);
        assertTrue(employees.size() == 20);
    }

    @Test
    public void testSave() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("Test" , "Testov" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setDivId(10);
        employeesDAO.save(employee);
        List<Employee> employees = employeesDAO.searchEmployees("Test");
        assertTrue(employees.size() > 0);
        assertEquals("Testov" , employees.get(0).getLastName());
        employeesDAO.delete(employees.get(0).getId());
    }

    @Test
    public void testUpdate() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("Sidor-Test" , "Sidorov-Testov" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setDivId(10);
        employee.setId(3);
        employeesDAO.update(employee);
        Employee employee1 = employeesDAO.getById(3);
        assertEquals("Sidor-Test" , employee1.getFirstName());
        employee1.setFirstName("Sidor");
        employee1.setLastName("Sidorov");
        employeesDAO.update(employee1);
        Employee employee2 = employeesDAO.getById(3);
        assertEquals("Sidor" , employee2.getFirstName());
    }

    @Test
    public void testDelete() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("Test" , "Testov" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setDivId(10);
        employeesDAO.save(employee);
        List<Employee> employees = employeesDAO.searchEmployees("Test");
        assertTrue(employees.size() > 0);
        assertEquals("Testov" , employees.get(0).getLastName());
        employeesDAO.delete(employees.get(0).getId());
        List<Employee> employees1 = employeesDAO.searchEmployees("Test");
        assertTrue(employees1.size() == 0);
    }

    @Test
    public void testGetAllOfEmployees() throws Exception {
        List<Employee> employees = employeesDAO.getAllOfEmployees();
        assertTrue(employees.size() > 1000);
    }
}