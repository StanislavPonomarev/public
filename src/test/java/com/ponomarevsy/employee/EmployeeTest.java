package com.ponomarevsy.employee;

import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import static junit.framework.Assert.*;


public class EmployeeTest {

    @Test
    public void testGetId() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setId(22);
        assertEquals(22 , employee.getId());
    }

    @Test
    public void testSetId() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setId(22);
        assertEquals(22 , employee.getId());
        employee.setId(33);
        assertEquals(33 , employee.getId());
    }

    @Test
    public void testGetFirstName() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        assertEquals("John" , employee.getFirstName());
    }

    @Test
    public void testSetFirstName() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setFirstName("  rAndY ");
        assertEquals("Randy" , employee.getFirstName());
    }

    @Test
    public void testGetLastName() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        assertEquals("Smith" , employee.getLastName());
    }

    @Test
    public void testSetLastName() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setLastName("  COnNoR  ");
        assertEquals("Connor" , employee.getLastName());
    }

    @Test
    public void testGetSalary() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        double salaryTest = 5500.75d;
        assertEquals(BigDecimal.valueOf(salaryTest) , employee.getSalary());
    }

    @Test
    public void testSetSalary() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setSalary(BigDecimal.valueOf(9990.99));
        assertEquals(BigDecimal.valueOf(9990.99) , employee.getSalary());
    }

    @Test
    public void testGetBirthDate() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        assertEquals(date , employee.getBirthDate());
    }

    @Test
    public void testSetBirthDate() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        Date testDate = simpleDateFormat.parse("25.03.1990");
        employee.setBirthDate(testDate);
        assertEquals(testDate , employee.getBirthDate());
    }

    @Test
    public void testIsActive() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        assertEquals(true , employee.isActive());
    }

    @Test
    public void testSetActive() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setActive(false);
        assertEquals(false , employee.isActive());
    }

    @Test
    public void testGetDivId() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setDivId(10);
        assertEquals(10 , employee.getDivId());
    }

    @Test
    public void testSetDivId() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        employee.setDivId(9);
        long testDivId = 7;
        employee.setDivId(testDivId);
        assertEquals(7 , employee.getDivId());
    }
}