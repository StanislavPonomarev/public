package com.ponomarevsy.employee;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import static junit.framework.Assert.*;

public class EmployeeFormValidatorTest {

    @Test
    public void testValidate() throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse("20.02.1985");
        Employee employee = new Employee("John" , "Smith" , BigDecimal.valueOf(5500.75) , date , true);
        Validator validator = new EmployeeFormValidator();

        BindingResult errorsFirstName = new BeanPropertyBindingResult(employee , "firstName");
        BindingResult errorsLastName = new BeanPropertyBindingResult(employee , "lastName");
        BindingResult errorsSalary = new BeanPropertyBindingResult(employee , "salary");
        BindingResult errorsBirthDate = new BeanPropertyBindingResult(employee , "birthDate");
        employee.setFirstName(" ");
        validator.validate(employee , errorsFirstName);
        assertEquals("required.firstName" , errorsFirstName.getFieldError("firstName").getCode());
        employee.setLastName(" ");
        validator.validate(employee , errorsLastName);
        assertEquals("required.lastName" , errorsLastName.getFieldError("lastName").getCode());
        employee.setSalary(null);
        validator.validate(employee, errorsSalary);
        assertEquals("required.salary" , errorsSalary.getFieldError("salary").getCode());
        employee.setBirthDate(null);
        validator.validate(employee, errorsBirthDate);
        assertEquals("required.birthDate" , errorsBirthDate.getFieldError("birthDate").getCode());

        BindingResult errorsFirstName1 = new BeanPropertyBindingResult(employee , "firstName");
        employee.setFirstName("dsfsfПывывуsdf4542323dsccv!2322@#");
        validator.validate(employee , errorsFirstName1);
        assertEquals("typeMismatch.firstName" , errorsFirstName1.getFieldError("firstName").getCode());

        BindingResult errorsLastName1 = new BeanPropertyBindingResult(employee , "lastName");
        employee.setLastName(" dfdfere456948es2333%2qw3ЫВцка !шнпр");
        validator.validate(employee , errorsLastName1);
        assertEquals("typeMismatch.lastName" , errorsLastName1.getFieldError("lastName").getCode());

        BindingResult errorsSalary1 = new BeanPropertyBindingResult(employee , "salary");
        employee.setSalary(BigDecimal.valueOf(777.999));
        validator.validate(employee , errorsSalary1);
        assertEquals("typeMismatch.salary" , errorsSalary1.getFieldError("salary").getCode());

        BindingResult errorsSalary2 = new BeanPropertyBindingResult(employee , "salary");
        employee.setSalary(BigDecimal.valueOf(-100.00));
        validator.validate(employee, errorsSalary2);
        assertEquals("typeMismatch.salary" , errorsSalary2.getFieldError("salary").getCode());

        BindingResult errorsBirthDate1 = new BeanPropertyBindingResult(employee , "birthDate");
        Date date1 = simpleDateFormat.parse("20.02.2016");
        employee.setBirthDate(date1);
        validator.validate(employee, errorsBirthDate1);
        assertEquals("date.format" , errorsBirthDate1.getFieldError("birthDate").getCode());
    }
}