package com.ponomarevsy.division;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static junit.framework.Assert.*;

@ContextConfiguration(locations = {"/applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class DivisionsDAOTest {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private DivisionsDAO divisionsDAO;

    @Test
    public void testGetById() throws Exception {
        Division division = divisionsDAO.getById(1);
        assertEquals("CEO" , division.getDivName());
    }

    @Test
    public void testSearchDivisions() throws Exception {
        List<Division> divisions = divisionsDAO.searchDivisions("Marketing");
        assertNotNull(divisions);
        assertEquals("Marketing" , divisions.get(0).getDivName());
    }

    @Test
    public void testGetAllDivisions() throws Exception {
        List<Division> divisions = divisionsDAO.getAllDivisions(1);
        assertNotNull(divisions);
        assertTrue(divisions.size() > 10);
    }

    @Test
    public void testSave() throws Exception {
        Division division = new Division("Test Division");
        divisionsDAO.save(division);
        List<Division> divisions = divisionsDAO.searchDivisions("Test Division");
        assertNotNull(divisions);
        assertTrue(divisions.get(0).getDivName().equals("Test Division"));
        for(Division division1 : divisions) {
            divisionsDAO.delete(division1.getId());
        }
    }

    @Test
    public void testUpdate() throws Exception {
    Division division = new Division("Back-office-Tested");
        division.setId(10);
        divisionsDAO.update(division);
        Division division1 = divisionsDAO.getById(10);
        assertEquals("Back-office-Tested", division1.getDivName());
        division1.setDivName("Back-office");
        divisionsDAO.update(division1);
    }

    @Test
    public void testDelete() throws Exception {
        Division division = new Division("New Test Division");
        divisionsDAO.save(division);
        List<Division> divisions = divisionsDAO.searchDivisions("New Test Division");
        assertTrue(divisions.size() > 0);
        divisionsDAO.delete(divisions.get(0).getId());
        List<Division> divisions1 = divisionsDAO.searchDivisions("New Test Division");
        assertTrue(divisions1.size() == 0);
    }

    @Test
    public void testGetAllOfDivisions() throws Exception {
        List<Division> divisions;
        divisions = divisionsDAO.getAllOfDivisions();
        assertNotNull(divisions);
        assertTrue(divisions.size() > 10);
    }
}