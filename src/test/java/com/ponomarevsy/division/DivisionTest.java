package com.ponomarevsy.division;

import org.junit.Test;
import static junit.framework.Assert.*;

public class DivisionTest {

    @Test
    public void testGetId() throws Exception {
        Division division = new Division("Test division");
        division.setId(1);
        assertEquals(1 , division.getId());
    }

    @Test
    public void testSetId() throws Exception {
        Division division = new Division("Test division");
        division.setId(1);
        long testId = division.getId();
        division.setId(testId);
        assertEquals(testId , division.getId());
        assertEquals(1, division.getId());
    }

    @Test
    public void testGetDivName() throws Exception {
        Division division = new Division("Test division");
        assertEquals("Test division" , division.getDivName());
    }

    @Test
    public void testSetDivName() throws Exception {
        Division division = new Division("Test Division");
        division.setDivName("  New Test Division  ");
        assertEquals("New Test Division" , division.getDivName());
    }
}