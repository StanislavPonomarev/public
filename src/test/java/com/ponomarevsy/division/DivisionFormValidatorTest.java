package com.ponomarevsy.division;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import static junit.framework.Assert.*;

public class DivisionFormValidatorTest {

    @Test
    public void testValidate() throws Exception {
        Division division = new Division("   ");
        Validator validator = new DivisionFormValidator();
        BindingResult errors = new BeanPropertyBindingResult(division , "divName");
        validator.validate(division , errors);
        assertEquals("required.divName" , errors.getFieldError("divName").getCode());
    }
}