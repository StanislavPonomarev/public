﻿function go(url)
{
        window.location = url;
}

function newDivision()
{
        window.location = "saveDivision.do";
}

function deleteDivision(url)
{
        var isOK = confirm("Вы действительно хотите удалить это подразделение? Если в справочнике сотрудников есть ссылки на данное подразделение, удаление не будет выполнено!" );
        if(isOK)
        {
                go(url);
        }
}
