function go(url)
{
        window.location = url;
}

function newEmployee()
{
        window.location = "saveEmployee.do";
}

function deleteEmployee(url)
{
        var isOK = confirm("Do you want to delete an employee?");
        if(isOK)
        {
                go(url);
        }
}
