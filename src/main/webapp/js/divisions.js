function go(url)
{
        window.location = url;
}

function newDivision()
{
        window.location = "saveDivision.do";
}

function deleteDivision(url)
{
        var isOK = confirm("Do you want to delete a division? Delete will give not effect , if this division has references in employees table" );
        if(isOK)
        {
                go(url);
        }
}
