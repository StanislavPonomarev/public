<%@include file="taglib_includes.jsp" %>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<title><spring:message code="App.Title"></spring:message> </title>

<script type="text/javascript" src="js/divisions.js"></script>
</head>
<body style="font-family: Arial; font-size:smaller;">

<h2><spring:message code="label.divisions" /></h2>

       <%@include file="showDivisionsMenu.jsp" %>

        <table style="border-collapse: collapse;" border="1" bordercolor="grey" width="100%">
                <tr bgcolor="lightgrey">
                        <th align = "center"><spring:message code="label.id" /></th>
                        <th align = "center"><spring:message code="label.divName" /></th>
                        <th align = "center"><spring:message code="label.actions" /></th>
                </tr>
                <c:if test="${empty SEARCH_DIVISIONS_RESULTS_KEY}">
                <tr>
                        <td colspan="4"><spring:message code="label.no_divisions_found" /></td>
                </tr>
                </c:if>
                <c:if test="${! empty SEARCH_DIVISIONS_RESULTS_KEY}">
                        <c:forEach var="division" items="${SEARCH_DIVISIONS_RESULTS_KEY}">
                    <tr>
                        <td align = "center"><c:out value="${division.id}"></c:out></td>
                        <td align = "center"><c:out value="${division.divName}"></c:out></td>

                                <td align = "center">
                                        &nbsp;<a href="updateDivision.do?id=${division.id}"><spring:message code="label.edit_div" /></a>

                                </td>
                        </tr>

                        </c:forEach>
                </c:if>

        </table>

        </center>


   </br>
        <c:forEach begin="${pageNum - 20 > 0?pageNum - 20:1}" end="${DIVISIONS_PAGE_COUNT}" var="p">
            <a style ="font-size:10px" href="viewAllDivisions.do?pageNum=${p}">${p}</a>
        </c:forEach>

</body>
</html>