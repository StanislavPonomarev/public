<%@include file="taglib_includes.jsp" %>
<html>
<head>
        <script type="text/javascript" src="js/divisions.js"></script>
        <title><spring:message code="App.Title"></spring:message> </title>
</head>
<body style="font-family: Arial; font-size:smaller;">

<table   width="100%" height="auto"  style="border-collapse: collapse;" border="1" bordercolor="grey" >
        <tr>
                <td  bgcolor="lightgrey"><b><spring:message code="label.edit_division" /></b></td>
        </tr>
  <tr valign="top">
    <td >
                 <form:form action="updateDivision.do" method="post" commandName="editDivision">
                                <table width="80%" style="border-collapse: collapse;" border="0" bordercolor="#006699" cellspacing="2" cellpadding="2">
                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.id" /></td>
                                                <td width="200">
                                                <form:input path="id" readonly="true"/></td>
                                                <td align="left">
                                                  </td>
                                        </tr>
                                        <tr>
                                                <td  align="right"><spring:message code="label.divName" /></td>
                                                <td>
                                                <form:input path="divName"/></td>
                                                <td align="left">
                                                <form:errors path="divName" cssStyle="color:red"></form:errors>
                                                </td>
                                        </tr>

                                        <tr valign="bottom">
                                                <td colspan="3" align="center">
                                                <input type="button"  value="<spring:message code="label.delete"/>" onclick="javascript:deleteDivision('deleteDivision.do?id=${editDivision.id}');">
                                                &nbsp;&nbsp;
                                                <input type="submit" name="" value="<spring:message code="label.save" />">
                                                &nbsp;&nbsp;
                                                <input type="button"  value="<spring:message code="label.cancel" />" onclick="javascript:go('viewAllDivisions.do?pageNum=1');">
                                                </td>
                                        </tr>

                                </table>
                </form:form>
    </td>
  </tr>
</table>

</body>
</html>
