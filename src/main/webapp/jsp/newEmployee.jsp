<%@include file="taglib_includes.jsp" %>
<html>
<head>
        <script type="text/javascript" src="js/employees.js"></script>
        <title><spring:message code="App.Title"></spring:message> </title>
</head>
<body style="font-family: Arial; font-size:smaller;">

<table   width="100%" height="auto" align="center" style="border-collapse: collapse;" border="1" bordercolor="grey" >
        <tr bgcolor="lightgrey">
                <td ><b><spring:message code="label.new_employee" /></b></td>
        </tr>
        <tr valign="top" >
    <td >
                 <form:form action="saveEmployee.do" method="post" commandName="newEmployee">

                                <table width="80%" style="border-collapse: collapse;" border="0" bordercolor="#006699" cellspacing="2" cellpadding="2">
                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.firstname" /></td>
                                                <td width="150">
                                                <form:input path="firstName"/></td>
                                                <td align="left">
                                                <form:errors path="firstName" cssStyle="color:red"></form:errors>
                                                </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.lastname" /></td>
                                                <td width="150">
                                                        <form:input path="lastName"/></td>
                                                <td align="left">
                                                        <form:errors path="lastName" cssStyle="color:red"></form:errors>
                                                </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.salary" /></td>
                                                <td><form:input path="salary"/></td>
                                                <td align="left"><form:errors path="salary"  cssStyle="color:red"></form:errors>  </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.birthDate" /></td>
                                                <td><form:input path="birthDate"/></td>
                                                <td align="left"><form:errors path="birthDate" cssStyle="color:red"></form:errors>  </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.divName" /></td>
                                                <td>

                                                        <form:select  path="divId" id="divisions" value = "1">
                                                                <c:forEach var="division" items="${ALL_DIVISION}">
                                                                        <form:option value="${division.id}" label="${division.divName}"/>
                                                                </c:forEach>

                                                        </form:select>

                                                </td>
                                                <td align="left">   </td>
                                        </tr>

                                        <tr>
                                                <td colspan="3" align="center">
                                                <input type="submit" name="" value="<spring:message code="label.save" />">
                                                &nbsp;&nbsp;
                                                <input type="button"  value="<spring:message code="label.cancel" />" onclick="javascript:go('viewAllEmployees.do?pageNum=1');">
                                                </td>
                                        </tr>
                                </table>
                </form:form>
    </td>
  </tr>
</table>
</body>
</html>
