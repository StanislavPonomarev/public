<%@include file="taglib_includes.jsp" %>
<html>
<head>
        <script type="text/javascript" src="js/employees.js"></script>
        <title><spring:message code="App.Title"></spring:message> </title>
</head>
<body style="font-family: Arial; font-size:smaller;">

<table   width="100%" height="auto"  style="border-collapse: collapse;" border="1" bordercolor="grey" >
        <tr>
                <td  bgcolor="lightgrey"><b><spring:message code="label.edit_employee" /></b></td>
        </tr>
  <tr valign="top">
    <td >
                 <form:form action="updateEmployee.do" method="post" commandName="editEmployee">
                                <table width="80%" style="border-collapse: collapse;" border="0" bordercolor="#006699" cellspacing="2" cellpadding="2">
                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.id" /></td>
                                                <td width="150">
                                                <form:input path="id" readonly="true"/></td>
                                                <td align="left">
                                                <form:errors path="id" cssStyle="color:red"></form:errors>  </td>
                                        </tr>
                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.firstname" /></td>
                                                <td>
                                                <form:input path="firstName"/></td>
                                                <td align="left">
                                                <form:errors path="firstName" cssStyle="color:red"></form:errors>
                                                </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.lastname" /></td>
                                                <td>
                                                        <form:input path="lastName"/></td>
                                                <td align="left">
                                                        <form:errors path="lastName" cssStyle="color:red"></form:errors>
                                                </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.salary" /></td>
                                                <td><form:input path="salary"/></td>
                                                <td align="left"><form:errors path="salary" cssStyle="color:red"></form:errors>  </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.birthDate" /></td>
                                                <td><form:input path="birthDate"/></td>
                                                <td align="left"><form:errors path="birthDate" cssStyle="color:red"></form:errors>  </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.active" /></td>
                                                <td>
                                                        <form:select path="active">
                                                    <form:option value="true" label="true"/>
                                                    <form:option value="false" label="false"/>
                                                </form:select>
                                                </td>
                                                <td>
                                                </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.divName" /></td>
                                                <td>

                                                        <form:select multiple="true" path="divId">
                                                                <c:forEach var="division" items="${ALL_DIVISION}">
                                                                        <form:option value="${division.id}" label="${division.divName}"/>
                                                                 </c:forEach>
                                                        </form:select>

                                                <td align="left">  </td>

                                                </td>
                                                <td align="left">  </td>
                                        </tr>

                                        <tr valign="bottom">
                                                <td colspan="3" align="center">
                                                <input type="button"  value="<spring:message code="label.delete" />" onclick="javascript:deleteEmployee('deleteEmployee.do?id=${editEmployee.id}');">
                                                &nbsp;&nbsp;
                                                <input type="submit" name="" value="<spring:message code="label.save" />">
                                                &nbsp;&nbsp;
                                                <input type="button"  value="<spring:message code="label.cancel" />" onclick="javascript:go('viewAllEmployees.do?pageNum=${PAGE_NUM}');">
                                                </td>
                                        </tr>

                                </table>
                </form:form>
    </td>
  </tr>
</table>


</body>
</html>
