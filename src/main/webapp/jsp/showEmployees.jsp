<%@include file="taglib_includes.jsp" %>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<title><spring:message code="App.Title"></spring:message> </title>

<script type="text/javascript" src="js/employees.js"></script>
</head>
<body style="font-family: Arial; font-size:smaller;">

<h2><spring:message code="label.employees" /></h2>

<%@include file="showEmployeesMenu.jsp" %>

        <table style="border-collapse: collapse;" border="1" bordercolor="grey" width="100%">
                <tr bgcolor="lightgrey">
                        <th align = "center"><spring:message code="label.id" /></th>
                        <th align = "center"><spring:message code="label.first_and_lastname" /></th>
                        <th align = "center"><spring:message code="label.actions" /></th>


                </tr>
                <c:if test="${empty SEARCH_EMPLOYEES_RESULTS_KEY}">
                <tr>
                        <td colspan="4"><spring:message code="label.no_employees_found" /></td>
                </tr>
                </c:if>
                <c:if test="${! empty SEARCH_EMPLOYEES_RESULTS_KEY}">
                        <c:forEach var="employee" items="${SEARCH_EMPLOYEES_RESULTS_KEY}">
                    <tr>
                        <td align = "center"><c:out value="${employee.id}"></c:out></td>
                        <td align = "center"><a href="detailEmployee.do?id=${employee.id}"><c:out value="${employee.firstName}"></c:out></a> <a href="detailEmployee.do?id=${employee.id}"><c:out value="${employee.lastName}"></c:out></a></td>
                        <td align = "center">&nbsp;<a href="updateEmployee.do?id=${employee.id}">Edit an employee</a>                        </td>

                        </tr>

                        </c:forEach>
                </c:if>


        </table>


        </center>


   </br>
        <c:forEach begin="${pageNum - 20 > 0?pageNum - 20:1}" end="${EMPLOYEES_PAGE_COUNT}" var="p">
            <a style ="font-size:10px" href="viewAllEmployees.do?pageNum=${p}">${p}</a>
        </c:forEach>

</body>
</html>