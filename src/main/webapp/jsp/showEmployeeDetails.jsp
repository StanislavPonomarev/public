<%@include file="taglib_includes.jsp" %>
<html>
<head>
        <script type="text/javascript" src="js/employees.js"></script>
        <title><spring:message code="App.Title"></spring:message> </title>
</head>
<body style="font-family: Arial; font-size:smaller;">

<table   width="100%" height="auto"  style="border-collapse: collapse;" border="1" bordercolor="grey" >
        <tr>
                <td  bgcolor="lightgrey"><b><spring:message code="label.employee_details" /></b></td>
        </tr>
  <tr valign="top">
    <td >
                 <form:form action="detailEmployee.do" method="post" commandName="showEmployeeDetails">
                                <table width="80%" style="border-collapse: collapse;" border="0" bordercolor="#006699" cellspacing="2" cellpadding="2">
                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.id" /></td>
                                                <td >
                                                <form:input path="id" readonly="true"/></td>
                                                <td align="left">
                                                  </td>
                                        </tr>
                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.firstname" /></td>
                                                <td>
                                                <form:input path="firstName" readonly="true"/></td>
                                                <td align="left">

                                                </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.lastname" /></td>
                                                <td>
                                                        <form:input path="lastName" readonly="true"/></td>

                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.salary" /></td>
                                                <td><form:input path="salary" readonly="true"/></td>
                                                <td align="left">  </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.birthDate" /></td>
                                                <td><form:input path="birthDate" readonly="true"/></td>
                                                <td align="left"> </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.active" /></td>
                                                <td>
                                                        <form:input path="active" readonly="true"/>

                                                </td>
                                                <td>
                                                </td>
                                        </tr>

                                        <tr>
                                                <td width="200" align="right"><spring:message code="label.divName" /></td>

                                                <td>
                                                        <form:select path="divId" readonly="true">
                                                                <c:forEach var="division" items="${DIVISIONS_ALL}">

                                                                        <form:option  value="${division.id}" label="${division.divName}"/>

                                                                </c:forEach>

                                                        </form:select>

                                                </td>

                                                <td align="left"> </td>
                                        </tr>



                                        <tr valign="bottom">
                                                <td colspan="3" align="center">
                                                &nbsp;&nbsp;
                                                <input type="button"  value="<spring:message code="label.cancel" />" onclick="javascript:go('viewAllEmployees.do?pageNum=${PAGE_NUM}');">
                                                </td>
                                        </tr>

                                </table>
                </form:form>
    </td>
  </tr>
</table>


</body>
</html>
