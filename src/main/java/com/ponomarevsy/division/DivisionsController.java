package com.ponomarevsy.division;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import com.ponomarevsy.employee.Employee;
import com.ponomarevsy.employee.EmployeesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class DivisionsController
{
        @Autowired
        private DivisionsDAO divisionsDAO;
        @Autowired
        private EmployeesDAO employeesDAO;
        @Autowired
        private DivisionFormValidator validator;
        @RequestMapping("/home1")
        public String home()
        {
                return "home1";
        }
        //-----------------------
        @InitBinder
        public void initBinder(WebDataBinder binder)
        {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateFormat.setLenient(false);
                binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        }
        //-----------------------
        @RequestMapping("/searchDivisions")
        public ModelAndView searchDivisions(@RequestParam(required= false, defaultValue="") String divName)
        {
                ModelAndView modelAndView = new ModelAndView("showDivisions");
                List<Division> divisions = divisionsDAO.searchDivisions(divName.trim());
                modelAndView.addObject("SEARCH_DIVISIONS_RESULTS_KEY", divisions);
                return modelAndView;
        }
        //-----------------------
        @RequestMapping(value = "/viewAllDivisions" , method=RequestMethod.GET)
        public ModelAndView getAllDivisions(@RequestParam(value = "pageNum", required = false) int pageNum)
        {
                ModelAndView modelAndView = new ModelAndView("showDivisions");
                List<Division> divisions = divisionsDAO.getAllDivisions(pageNum);
                modelAndView.addObject("SEARCH_DIVISIONS_RESULTS_KEY", divisions);
                int pageCount = 0;
                List<Division> divisionsAll = divisionsDAO.getAllOfDivisions();
                if (divisionsAll.size()%20 == 0)
                        pageCount = divisionsAll.size()/20;
                else pageCount = divisionsAll.size()/20 + 1;
                modelAndView.addObject("DIVISIONS_PAGE_COUNT" , pageCount);
                return modelAndView;
        }
        //-----------------------
        @RequestMapping(value="/saveDivision", method=RequestMethod.GET)
        public ModelAndView newDivisionForm()
        {
                ModelAndView modelAndView = new ModelAndView("newDivision");
                Division division = new Division();
                modelAndView.getModelMap().put("newDivision", division);
                return modelAndView;
        }
        //-----------------------
        @RequestMapping(value="/saveDivision", method=RequestMethod.POST)
        public String create(@ModelAttribute("newDivision")Division division, BindingResult result, SessionStatus status)
        {
                List<Division> divisions = divisionsDAO.getAllOfDivisions();
                String divName = division.getDivName().trim().toLowerCase();
                for(Division division1:divisions) {
                        if(division1.getDivName().toLowerCase().equalsIgnoreCase(divName)) {
                                result.rejectValue("divName","validation.duplicate");
                        }
                }
                validator.validate(division, result);
                if (result.hasErrors())
                {
                        return "newDivision";
                }
                divisionsDAO.save(division);
                status.setComplete();
                return "redirect:viewAllDivisions.do?pageNum=1";
        }
        //-----------------------
        @RequestMapping(value="/updateDivision", method=RequestMethod.GET)
        public ModelAndView edit(@RequestParam("id")Integer id)
        {
                ModelAndView modelAndView = new ModelAndView("editDivision");
                Division division = divisionsDAO.getById(id);
                modelAndView.addObject("editDivision", division);
                return modelAndView;
        }
        //-----------------------
        @RequestMapping(value="/updateDivision", method=RequestMethod.POST)
        public String update(@ModelAttribute("editDivision") Division division, BindingResult result, SessionStatus status)
        {
                List<Division> divisions = divisionsDAO.getAllOfDivisions();
                String divName = division.getDivName().trim().toLowerCase();
                for(Division division1:divisions) {
                        if(division1.getDivName().toLowerCase().equalsIgnoreCase(divName) && ( division1.getId() != division.getId() ) ) {
                                result.rejectValue("divName","validation.duplicate");
                        }
                }
                validator.validate(division, result);
                if (result.hasErrors()) {
                        return "editDivision";
                }
                divisionsDAO.update(division);
                status.setComplete();
                return "redirect:viewAllDivisions.do?pageNum=1";
        }
        //-----------------------
        @RequestMapping("deleteDivision")
        public ModelAndView delete(@RequestParam("id")Integer id)
        {
                List<Employee> employees = employeesDAO.getAllOfEmployees();
                for(Employee employee : employees) {
                        if((int)employee.getDivId() == id) {
                                Division division = divisionsDAO.getById(id);
                                ModelAndView modelAndView = new ModelAndView("redirect:/updateDivision.do?id=" + division.getId());
                                return modelAndView;
                        }
                }
                ModelAndView modelAndView = new ModelAndView("redirect:viewAllDivisions.do?pageNum=1");
                divisionsDAO.delete(id);
                return modelAndView;
        }
}