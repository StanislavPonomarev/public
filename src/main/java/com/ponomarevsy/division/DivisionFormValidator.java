package com.ponomarevsy.division;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component("divisionFormValidator")
public class DivisionFormValidator implements Validator
{
        @SuppressWarnings("all")
        @Override
        public boolean supports(Class supportsClass)
        {
                return Division.class.isAssignableFrom(supportsClass);
        }
        //-----------------------
        @Override
        public void validate(Object divisionObject, Errors errors)
        {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "divName", "required.divName");
        }

}
