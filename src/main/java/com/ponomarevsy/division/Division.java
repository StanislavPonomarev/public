package com.ponomarevsy.division;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name="division")
public class Division
{
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id;
        @Column
        private String divName;

        public Division() {
        }

        public Division(String divName) {
                this.divName = divName;
        }

        @Override
        public String toString()
        {
                return ToStringBuilder.reflectionToString(this);
        }

        public long getId() {
                return id;
        }

        public void setId(long id) {
                this.id = id;
        }

        public String getDivName() {
                return divName;
        }

        public void setDivName(String divName) {
               this.divName = divName.trim();
        }

}
