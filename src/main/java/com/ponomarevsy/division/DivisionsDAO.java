package com.ponomarevsy.division;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
@Transactional
public class DivisionsDAO
{
        @Autowired
        private SessionFactory sessionFactory;
        //-----------------------
        public Division getById(long id)
        {
                return (Division) sessionFactory.getCurrentSession().get(Division.class, id);
        }
        //-----------------------
        @SuppressWarnings("unchecked")
        public List<Division> searchDivisions(String divName)
        {
                Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Division.class);
                criteria.add(Restrictions.ilike("divName", divName + "%"));
                return criteria.list();
        }
        //-----------------------
        @SuppressWarnings("unchecked")
        public List<Division> getAllDivisions(int pageNumber)
        {
                Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Division.class);
                criteria = criteria.setFirstResult(20 * (pageNumber - 1));
                criteria.setMaxResults(20);
                return criteria.list();
        }
        //-----------------------
        public long save(Division division)
        {
                return (Long)sessionFactory.getCurrentSession().save(division);
        }
        //-----------------------
        public void update(Division division)
        {
                sessionFactory.getCurrentSession().merge(division);
        }
        //-----------------------
        public void delete(long id)
        {
                Division division = getById(id);
                sessionFactory.getCurrentSession().delete(division);
        }
        //-----------------------
        public List<Division> getAllOfDivisions()
        {
                Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Division.class);
                return criteria.list();
        }
}