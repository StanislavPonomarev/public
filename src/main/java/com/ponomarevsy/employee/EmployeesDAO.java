package com.ponomarevsy.employee;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@SuppressWarnings("all")
public class EmployeesDAO
{
        @Autowired
        private SessionFactory sessionFactory;

        //-----------------------
        public Employee getById(long id)
        {
                return (Employee) sessionFactory.getCurrentSession().get(Employee.class, id);
        }
        //-----------------------
        @SuppressWarnings("unchecked")
        public List<Employee> searchEmployees(String firstName)
        {
                Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Employee.class);
                criteria.add(Restrictions.ilike("firstName", firstName + "%"));
                return criteria.list();
        }
        @SuppressWarnings("unchecked")
        public List<Employee> getAllEmployees(int pageNumber)
        {
                Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Employee.class);
                criteria = criteria.setFirstResult(20 * (pageNumber - 1));
                criteria.setMaxResults(20);
                return criteria.list();
        }
        //-----------------------
        public long save(Employee employee)
        {
                     return (Long) sessionFactory.getCurrentSession().save(employee);
        }
        //-----------------------
        public void update(Employee employee)
        {

                sessionFactory.getCurrentSession().merge(employee);
        }
        //-----------------------
        public void delete(long id)
        {
                Employee employee = getById(id);
                sessionFactory.getCurrentSession().delete(employee);
        }
        //-----------------------
        public List<Employee> getAllOfEmployees()
        {
                Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Employee.class);
                return criteria.list();
        }
}
