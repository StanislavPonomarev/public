package com.ponomarevsy.employee;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import com.ponomarevsy.division.Division;
import com.ponomarevsy.division.DivisionsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@SuppressWarnings("all")
public class EmployeesController
{
        @Autowired
        private EmployeesDAO employeesDAO;

        @Autowired
        private DivisionsDAO divisionsDAO;

        @Autowired
        private EmployeeFormValidator validator;

        @RequestMapping("/home")
        public String home()
        {
                return "home";
        }
    //-----------------------
        @InitBinder
        public void initBinder(WebDataBinder binder)
        {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                dateFormat.setLenient(false);
                binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        }
    //-----------------------
        @RequestMapping("/searchEmployees")
        public ModelAndView searchEmployees(@RequestParam(required= false, defaultValue="") String firstNameAndLastName)
        {
                ModelAndView modelAndView = new ModelAndView("showEmployees");
                List<Employee> employees = employeesDAO.getAllOfEmployees();
                List<Employee> employees1 = new ArrayList<Employee>();
                firstNameAndLastName = "^" + firstNameAndLastName.replace( " ", "" ).toLowerCase().replace("*" ,".*").replace("?",".?") + "$";
                Pattern pattern = Pattern.compile(firstNameAndLastName);
                for(Employee employee : employees) {
                        String firstNameAndLastNameFromBase = employee.getFirstName() + employee.getLastName();
                        firstNameAndLastNameFromBase = firstNameAndLastNameFromBase.toLowerCase();
                        String firstNameFromBase = employee.getFirstName().toLowerCase();
                        String lastNameFromBase = employee.getLastName().toLowerCase();
                        Matcher matcherToFirstAndLastName = pattern.matcher(firstNameAndLastNameFromBase);
                        Matcher matcherToFirstName = pattern.matcher(firstNameFromBase);
                        Matcher matcherToLastName = pattern.matcher(lastNameFromBase);
                        if(matcherToFirstAndLastName.matches()) {
                                employees1.add(employee);
                        }
                        else if(matcherToFirstName.matches()) {
                                employees1.add(employee);
                        }
                        else if(matcherToLastName.matches()) {
                                employees1.add(employee);
                        }
                }
                modelAndView.addObject("SEARCH_EMPLOYEES_RESULTS_KEY", employees1);
                return modelAndView;
        }
    //-----------------------
        @RequestMapping(value = "/viewAllEmployees" , method=RequestMethod.GET)
        public ModelAndView getAllEmployees(@RequestParam(value = "pageNum", required = false) int pageNum)
        {
                ModelAndView modelAndView = new ModelAndView("showEmployees");
                List<Employee> employees = employeesDAO.getAllEmployees(pageNum);
                modelAndView.addObject("SEARCH_EMPLOYEES_RESULTS_KEY", employees);
                int pageCount = 0;
                List<Employee> employeesAll = employeesDAO.getAllOfEmployees();
                if (employeesAll.size()%20 == 0)
                        pageCount = employeesAll.size()/20;
                else pageCount = employeesAll.size()/20 + 1;
                modelAndView.addObject("EMPLOYEES_PAGE_COUNT" , pageCount);
                return modelAndView;
        }
    //-----------------------
        @RequestMapping(value="/saveEmployee", method=RequestMethod.GET)
        public ModelAndView newEmployeeForm()
        {
                ModelAndView modelAndView = new ModelAndView("newEmployee");
                Employee employee = new Employee();
                modelAndView.getModelMap().put("newEmployee", employee);
                List<Division> divisionsAll = divisionsDAO.getAllOfDivisions();
                modelAndView.addObject("DIVISIONS_ALL", divisionsAll);
                return modelAndView;
        }
    //-----------------------
        @RequestMapping(value="/saveEmployee", method=RequestMethod.POST)
        public String create(@ModelAttribute("newEmployee")Employee employee, BindingResult result, SessionStatus status)
        {
            validator.validate(employee, result);
            if (result.hasErrors()) {
                return "newEmployee";
            }
            employeesDAO.save(employee);
            status.setComplete();
                //-----
                int pageNumber = 1;
                List<Employee> employees = employeesDAO.getAllOfEmployees();
                int employeesSize = employees.size();
                int elementIndex = 0;
                for(int i = 0 ; i < employees.size() ; i++) {
                        if(employees.get(i).getId() == employee.getId()) {
                                elementIndex = i;
                        }
                }
                elementIndex = elementIndex + 1;
                if(elementIndex%20 == 0) pageNumber = elementIndex/20;
                else pageNumber = elementIndex/20 + 1;
                if(employees.get(0).getId() == employee.getId()) pageNumber = 1;
                //-----
            return "redirect:viewAllEmployees.do?pageNum=" + pageNumber;
        }
    //-----------------------
        @RequestMapping(value="/updateEmployee", method=RequestMethod.GET)
        public ModelAndView edit(@RequestParam("id")Integer id)
        {
                ModelAndView modelAndView = new ModelAndView("editEmployee");
                Employee employee = employeesDAO.getById(id);
                modelAndView.addObject("editEmployee", employee);
                List<Division> divisionsAll = divisionsDAO.getAllOfDivisions();
                modelAndView.addObject("DIVISIONS_ALL" , divisionsAll);
                //-----
                int pageNumber = 1;
                List<Employee> employees = employeesDAO.getAllOfEmployees();
                int employeesSize = employees.size();
                int elementIndex = 0;
                for(int i = 0 ; i < employees.size() ; i++) {
                        if(employees.get(i).getId() == employee.getId()) {
                                elementIndex = i;
                        }
                }
                elementIndex = elementIndex + 1;
                if(elementIndex%20 == 0) pageNumber = elementIndex/20;
                else pageNumber = elementIndex/20 + 1;
                if(employees.get(0).getId() == employee.getId()) pageNumber = 1;
                modelAndView.addObject("PAGE_NUM" , pageNumber);
                //-----
                return modelAndView;
        }
    //-----------------------
        @RequestMapping(value="/updateEmployee", method=RequestMethod.POST)
        public String update(@ModelAttribute("editEmployee") Employee employee, BindingResult result, SessionStatus status)
        {
               validator.validate(employee, result);
                if (result.hasErrors()) {
                        return "editEmployee";
                }
                employeesDAO.update(employee);
                status.setComplete();
                //----
                int pageNumber = 1;
                List<Employee> employees = employeesDAO.getAllOfEmployees();
                int employeesSize = employees.size();
                int elementIndex = 0;
                for(int i = 0 ; i < employees.size() ; i++) {
                        if(employees.get(i).getId() == employee.getId()) {
                                elementIndex = i;
                        }
                }
                elementIndex = elementIndex + 1;
                if(elementIndex%20 == 0) pageNumber = elementIndex/20;
                else pageNumber = elementIndex/20 + 1;
                if(employees.get(0).getId() == employee.getId()) pageNumber = 1;
                //----
                return "redirect:viewAllEmployees.do?pageNum=" + pageNumber;
        }
    //-----------------------
        @RequestMapping("deleteEmployee")
        public ModelAndView delete(@RequestParam("id")Integer id)
        {
                //--
                Employee employee = employeesDAO.getById(id);
                int pageNumber = 1;
                List<Employee> employees = employeesDAO.getAllOfEmployees();
                int employeesSize = employees.size();
                int elementIndex = 0;
                for(int i = 0 ; i < employees.size() ; i++) {
                        if(employees.get(i).getId() == employee.getId()) {
                                elementIndex = i;
                        }
                }
                if(elementIndex%20 == 0) pageNumber = elementIndex/20;
                else pageNumber = elementIndex/20 + 1;
                if(employees.get(0).getId() == employee.getId()) pageNumber = 1;
                //--
                ModelAndView modelAndView = new ModelAndView("redirect:viewAllEmployees.do?pageNum=" + pageNumber);
                employeesDAO.delete(id);
                return modelAndView;
        }
    //-----------------------
        @RequestMapping(value="/detailEmployee", method=RequestMethod.GET)
        public ModelAndView view(@RequestParam("id")Integer id)
        {
                ModelAndView modelAndView = new ModelAndView("showEmployeeDetails");
                Employee employee = employeesDAO.getById(id);
                modelAndView.addObject("showEmployeeDetails", employee);
                List<Division> divisionsAll = divisionsDAO.getAllOfDivisions();
                modelAndView.addObject("DIVISIONS_ALL" , divisionsAll);
                //-----
                int pageNumber = 1;
                List<Employee> employees = employeesDAO.getAllOfEmployees();
                int employeesSize = employees.size();
                int elementIndex = 0;
                for(int i = 0 ; i < employees.size() ; i++) {
                        if(employees.get(i).getId() == employee.getId()) {
                                elementIndex = i;
                        }
                }
                elementIndex = elementIndex + 1;
                if(elementIndex%20 == 0) pageNumber = elementIndex/20;
                else pageNumber = elementIndex/20 + 1;
                if(employees.get(0).getId() == employee.getId()) pageNumber = 1;
                modelAndView.addObject("PAGE_NUM" , pageNumber);
                //-----
                return modelAndView;
        }
    //-----------------------
        @ModelAttribute("ALL_DIVISION")
        public List<Division> getDivs() {
                return divisionsDAO.getAllOfDivisions();
        }
}