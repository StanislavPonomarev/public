package com.ponomarevsy.employee;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("employeeFormValidator")

public class EmployeeFormValidator implements Validator
{
        @SuppressWarnings("all")
        @Override
        public boolean supports(Class supportsClass)
        {
                return Employee.class.isAssignableFrom(supportsClass);
        }

        @Override
        public void validate(Object object, Errors errors) {
               Employee employee = (Employee) object;
               ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "required.firstName");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "required.lastName");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salary", "required.salary");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "required.birthDate");

               if(isHasRequiredFormat(employee.getFirstName().toString() , "^[а-яА-ЯёЁa-zA-Z]+$") == false) {
                   errors.rejectValue("firstName", "typeMismatch.firstName");
               }
               if(isHasRequiredFormat(employee.getLastName().toString() , "^[а-яА-ЯёЁa-zA-Z]+$") == false) {
                   errors.rejectValue("lastName", "typeMismatch.lastName");
               }
               if(isHasRequiredFormat(String.valueOf(employee.getSalary()) , "^[0-9]{1,13}\\.[0-9]{2}$") == false ) {
                   errors.rejectValue("salary" , "typeMismatch.salary");
               }
               if(isHasRequiredFormat(String.valueOf(employee.getSalary()) , "^[0]{1,13}\\.[0]{2}$") == true) {
                   errors.rejectValue("salary" , "validation.null");
               }
               if(errors.hasFieldErrors("birthDate") == false) {
                   Date date = employee.getBirthDate();
                   SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                   dateFormat.format(date);
                   Calendar calendar = Calendar.getInstance();
                   calendar.setTime(date);
                   if ( calendar.get(Calendar.YEAR) < 1900 || (calendar.get(Calendar.YEAR) > new GregorianCalendar().get(Calendar.YEAR))) {
                       errors.rejectValue("birthDate", "date.format");
                   }
               }
        }
    //-----------------------
    public boolean isHasRequiredFormat(String sourceString , String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(sourceString.trim());
        return matcher.matches();
    }
}
