package com.ponomarevsy.employee;

/**
 * Created by Ponomarev Stanislav on 07.01.15.
 */

import org.apache.commons.lang.builder.ToStringBuilder;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="employee")
public class Employee
{
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id;
        @Column
        private String firstName;
        @Column
        private String lastName;
        @Column
        private BigDecimal salary;
        @Column
        @Temporal(value=TemporalType.DATE)
        private Date birthDate;
        @Column
        private boolean active;
        @Column
        private long divId;

        public Employee() {
                this.active = true;
        }

        public Employee(String firstName, String lastName, BigDecimal salary, Date birthDate, boolean active) {
                this.firstName = firstName;
                this.lastName = lastName;
                this.salary = salary;
                this.birthDate = birthDate;
                this.active = active;
        }

        @Override
        public String toString()
        {
                return ToStringBuilder.reflectionToString(this);
        }

        public long getId() {
                return id;
        }

        public void setId(long id) {
                this.id = id;
        }

        public String getFirstName() {
                return firstName;
        }

        public void setFirstName(String firstName) {
            if(firstName.trim().length() != 0) {
                    String trimmedfirstName = firstName.trim();
                    String modifiedFirstName = trimmedfirstName.substring(0, 1).toUpperCase() + trimmedfirstName.substring(1).toLowerCase();
                    this.firstName = modifiedFirstName;
            }
             else {
                    this.firstName = firstName;
            }
        }

        public String getLastName() {
                return lastName;
        }

        public void setLastName(String lastName) {
                if(lastName.trim().length() != 0) {
                        String trimmedLastName = lastName.trim();
                        String modifiedLastName = trimmedLastName.substring(0, 1).toUpperCase() + trimmedLastName.substring(1).toLowerCase();
                        this.lastName = modifiedLastName;
                }
                else {
                        this.lastName = lastName;
                }

        }

        public BigDecimal getSalary() {
                       return salary;
        }

        public void setSalary(BigDecimal salary) {
        this.salary = salary;
        }

        public Date getBirthDate() {
                return birthDate;
        }

        public void setBirthDate(Date birthDate) {
                this.birthDate = birthDate;
        }

        public boolean isActive() {
                return active;
        }

        public void setActive(boolean active) {
                this.active = active;
        }

        public long getDivId() {
                return divId;
        }

        public void setDivId(long divId) {
                this.divId = divId;
        }

}
